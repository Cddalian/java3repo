/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package effectivesorting;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author 1796146
 */
public class Person implements Comparable<Person> {

    String name;
    int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int compareTo(Person compared) {
        if (this.age == compared.age) {
            return 0;
        }
        else {
            return this.age > compared.age ? 1 : -1;
        }
    }

    static ArrayList<Person> people = new ArrayList<Person>();

    public static void main(String[] args) {

        Person person1 = new Person("John", 20);
        Person person2 = new Person("Bob", 42);
        Person person3 = new Person("Chris", 99);
        Person person4 = new Person("Sharon", 20);
        Person person5 = new Person("Tab", 102);
        Person person6 = new Person("Jose", 12);
        Person person7 = new Person("Mike", 45);

        people.add(person1);
        people.add(person2);
        people.add(person3);
        people.add(person4);
        people.add(person5);
        people.add(person6);
        people.add(person7);
        
        Collections.sort(people);
        
        for (Person p : people) {
            System.out.println("Name: " + p.name + " Age: " + p.age);
        }
  }

    
}
