/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package effectivesorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author D
 */
public class CarComparatorByMaxSpeed implements Comparator {

    
    @Override
    public int compare(Object o1, Object o2) {
               
        int car1MaxKph = ((Car) o1).maxSpeedKph;
        int car2MaxKph = ((Car) o2).maxSpeedKph;
        
        return car2MaxKph - car1MaxKph;
        /*
        if (car1MaxKph == car2MaxKph) {
            return 0;
        }
        else if (car1MaxKph > car2MaxKph) {
            return 1;
        }
        else return -1;
        */
    }

    /*
    public ArrayList<Car> sortBySpeed (ArrayList<Car> garage) {
        Collections.(garage);
        return garage;
    }
*/
}
