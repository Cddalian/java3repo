/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package effectivesorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author D
 */
public class Car implements Comparable<Car>{

    String make; // e.g. BMW, Toyota, etc.
    String model; // e.g. X5, Corolla, etc.
    int maxSpeedKph; // maximum speed in km/h
    double secTo100Kph; // accelleration time 1-100 km/h in seconds
    double litersPer100km; // economy: liters of gas per 100 km

    public Car(String make, String model, int maxSpeedKph, double secTo100Kph, double litersPer100km) {
        this.make = make;
        this.model = model;
        this.maxSpeedKph = maxSpeedKph;
        this.secTo100Kph = secTo100Kph;
        this.litersPer100km = litersPer100km;
    }
    static ArrayList<Car> garage = new ArrayList<>();

    @Override
    public int compareTo(Car compared) {
        if (this.make.compareTo(compared.make) == 0) {
            return 0;
        }
        else if (this.make.compareTo(compared.make) > 1) {
            return 1;
        } 
        else return -1;
    }
    
    public static void main(String[] args) {
        Car car1 = new Car("Honda", "Civic", 180, 10, 7);
        Car car2 = new Car("Honda", "Accord", 190, 9, 6);
        Car car3 = new Car("Subaru", "Legacy", 180, 11, 8);
        Car car4 = new Car("BMW", "Z8", 260, 6, 9);
        Car car5 = new Car("BMW", "328", 250, 6.5, 9);
        Car car6 = new Car("Toyota", "Camry", 240, 8, 7);
        Car car7 = new Car("Toyota", "Echo", 140, 16, 6);
        Car car8 = new Car("AMC", "Gremlin", 100, 52.9, 15);
        Car car9 = new Car("Ford", "Pinto", 120, 45, 16);
        Car car10 = new Car("DeLorean", "DMC-12", 150, 14, 11);
       
        garage.add(car1);
        garage.add(car2);
        garage.add(car3);
        garage.add(car4);
        garage.add(car5);
        garage.add(car6);
        garage.add(car7);
        garage.add(car8);
        garage.add(car9);
        garage.add(car10);
        /*
        Collections.sort(garage);   // sorted by make
        Collections.sort(garage, new Comparator() { // sorted by make and model (using comparator not comparable...

            public int compare(Object o1, Object o2) {

                String car1Make = ((Car) o1).make;
                String car2Make = ((Car) o2).make;
                int compare = car1Make.compareTo(car2Make);

                if (compare != 0) {
                   return compare;
                } 
                else {
                   String car1Model = ((Car) o1).model;
                   String car2Model = ((Car) o2).model;
                   return car1Model.compareTo(car2Model);
                }
            }
        });

        for (Car c : garage) {
            System.err.println(c.make + " " + c.model);
        }
        */

        Collections.sort(garage, new CarComparatorByMaxSpeed()); //this will access the compare method in that class

        for (Car c : garage) {
            System.err.println(c.make + " " + c.model + " " + c.maxSpeedKph);
        }
    }

    
}
