package geometry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

class SurfaceComparator implements Comparator<GeoObj> {

    @Override
    public int compare(GeoObj o1, GeoObj o2) {
        if (o1.getSurface() == o2.getSurface()) {
            return 0;   // if theyre equal
        }
        else if (o1.getSurface() < o2.getSurface()) {
            return -1; // if o1 is "less" than o2
        }
        else return 1;  // if o1 is greater than o2
    }
    
}

abstract class GeoObj implements Comparable<GeoObj> { //implement the Comparable interface, in <> you say what youre comparing TO

    String color;

    public GeoObj(String color) {
        this.color = color;
    }

    @Override   // if you implement Comparable, you need to override the method compareTo. again in the () you specify the object to compare to
    public int compareTo(GeoObj other) {
        //System.err.println("Compare to called from sort method");
        return color.compareTo(other.color);
    }

    abstract double getSurface();
    
    public static final Comparator<GeoObj> surfaceComparator = new SurfaceComparator();
}


class Rectangle extends GeoObj {

    double width;
    double height;

    public Rectangle(String color, double width, double height) {
        super(color);
        this.width = width;
        this.height = height;
    }

    @Override
    double getSurface() {
        return width * height;
    }
}

class Circle extends GeoObj {

    double radius;

    public Circle(String color, double radius) {
        super(color);
        this.radius = radius;
    }

    @Override
    double getSurface() {
        return Math.PI * (radius * radius);
    }

}

public class Geometry {

    public static void main(String[] args) {

        /*
         Rectangle r = new Rectangle("blue", 10, 10);
         System.err.println("Rectangle surface: " + r.getSurface());
         Circle c = new Circle("red", 0, 0, 1);
         System.err.println("Circle radius: " + c.getSurface());
         */
        ArrayList<GeoObj> objects = new ArrayList<>();
        objects.add(new Rectangle("Purple", 10, 20));
        objects.add(new Rectangle("Red", 20, 20));
        objects.add(new Rectangle("Yellow", 5, 5));
        objects.add(new Circle("Orange", 5));
        objects.add(new Circle("Azure", 10));

        for (GeoObj o : objects) {
            System.out.println(o.color);
        }
        Collections.sort(objects);
        System.out.println("After sorting: ");

        for (GeoObj o : objects) {
            System.out.println(o.color);
        }
        
        System.err.println("Sort by surface area");
        Collections.sort(list, GeoObj.surfaceComparator);
    }
}
