/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Trips;

import java.sql.Date;

/**
 *
 * @author 1796146
 */
public class Trip {
    Long id;
    String name;
    Date depDate;
    Date retDate;
    String dest;
    boolean isFirstClass;
    
    @Override
    public String toString() {
        return (id + ": " + name + " " + depDate.toString() + " " + retDate.toString() + " " + dest + " First Class: " + isFirstClass);
    }
}
