/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuizFlights;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author 1796146
 */
public class Database {
    
    private String url = "jdbc:sqlserver://cddb1jab.database.windows.net:1433;databaseName=cddb1;user=cddaldb1;password=testdb1jaB";  
    
    private Connection conn;  
    
    public Database() {
        try {    
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");  
            conn = DriverManager.getConnection(url); 
        } catch (SQLException e) {
            Logger.getLogger(FlightsGUI.class.getName()).log(Level.SEVERE, null, e);
            System.err.println("Error connecting to databse...");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FlightsGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.err.println("Connected");
    }
       
    
    public List<Flight> loadList() {
         
        String sql = "SELECT id, onDay, fromCode, toCode, type FROM flights;";
        List<Flight> list = new ArrayList<>();
        
        try {
           
            Statement st = conn.createStatement();
            
            ResultSet rs = st.executeQuery(sql);
            
            
            while (rs.next()) {
                Flight flight = new Flight();
                flight.id = rs.getInt("id");
                flight.onDay = rs.getString("onDay");
                flight.fromCode = rs.getString("fromCode");
                flight.toCode = rs.getString("toCode");
                try {
                    String flightType = rs.getString("type");
                    flight.type = Flight.Type.valueOf(flightType);
                } catch (IllegalArgumentException e) {
                    System.err.println("Internal error");
                }
                
                list.add(flight);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public void addFlight(Flight flight) {
        String sql = "INSERT INTO flights (onDay, fromCode, toCode, type) VALUES (?, ?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            
            stmt.setString(1, flight.getOnDay());
            stmt.setString(2, flight.getFromCode());
            stmt.setString(3, flight.getToCode());
            stmt.setString(4, flight.type.toString());

            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.err.println("Error adding flight to database");
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        } /*catch (ParseException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
    }
    
    public void deleteFlightById(long id) throws SQLException {
        String sql = "DELETE FROM flights WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            stmt.executeUpdate();
        }
    }
    
}
