/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuizFlights;

import java.util.Date;

/**
 *
 * @author 1796146
 */
public class Flight {
    
    long id;
    String onDay;
    String fromCode, toCode;    //airport code, ex. YUL
    Type type;  // enum, add constraint in db
    int passengers;

    enum Type { Domestic, International, Private };

            
    @Override
    public String toString() {
        return id + " On: " + onDay + " From: " + fromCode + " To: " + toCode + " Type: " + type + " Passengers: " + passengers;
    } 

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOnDay() {
        return onDay;
    }

    public void setOnDay(String onDay) {
        this.onDay = onDay;
    }

    public String getFromCode() {
        return fromCode;
    }

    public void setFromCode(String fromCode) {
        this.fromCode = fromCode;
    }

    public String getToCode() {
        return toCode;
    }

    public void setToCode(String toCode) {
        this.toCode = toCode;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }
    
    
}
