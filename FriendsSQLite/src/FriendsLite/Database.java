/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FriendsLite;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1796146
 */
public class Database {

    // private Connection conn;
 
    
    public static void createNewDatabase() {
 
        // String url = "jdbc:sqlite:/testdb1.sqlite";
 
        try (Connection conn = DriverManager.getConnection(FriendDB.url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void createNewTable() {
        // SQLite connection string
        // String url = "jdbc:sqlite:/testdb1.sqlite";
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS friends (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	gender text NOT NULL\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(FriendDB.url);
                Statement statement = conn.createStatement()) {
            // create a new table
            statement.execute(sql);
            System.err.println("Table Created...");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
