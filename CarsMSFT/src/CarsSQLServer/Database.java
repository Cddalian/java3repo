/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CarsSQLServer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1796146
 */
public class Database {
    
    private String url = "jdbc:sqlserver://cddb1jab.database.windows.net:1433;databaseName=cddb1;user=cddaldb1;password=testdb1jaB";  
    
    private Connection conn;  
    
    public Database() {
        try {  
            System.err.println(url);
         // Establish the connection.  
         /* Class.forName preloads the driver to make sure it is there. Apparently useful ONLY
         sometimes?? Use it to be on the safe side */
         
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");  
            conn = DriverManager.getConnection(url); 
        } catch (SQLException e) {
            Logger.getLogger(CarsGUI.class.getName()).log(Level.SEVERE, null, e);
            System.err.println("Error connecting");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CarsGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.err.println("Connected");
    }
       
    
    public List<Car> loadList() {
         
        String sql = "SELECT id, makeModel, engineSize, fuelType FROM cars;";
        List<Car> list = new ArrayList<>();
        
        try {
           
            Statement st = conn.createStatement();
            
            System.err.println(sql);
            ResultSet rs = st.executeQuery(sql);
            
            
            while (rs.next()) {
                Car car = new Car();
                car.id = rs.getInt("id");
                car.makeModel = rs.getString("makeModel");
                car.engineSize = rs.getDouble("engineSize");
                String fuelTypeStr = rs.getString("fuelType");
                car.fuelType = Car.FuelType.valueOf(fuelTypeStr);
                System.out.println(car.id + " " + car.makeModel + " ");
                list.add(car);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public void addCar(Car car) {
        String sql = "INSERT INTO cars (makeModel, engineSize, fuelType) VALUES (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, car.getMakeModel());
            stmt.setDouble(2, car.getEngineSize());
            stmt.setString(3, car.fuelType.toString());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void deleteCarById(long id) throws SQLException {
        String sql = "DELETE FROM cars WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            stmt.executeUpdate();
        }
    }
    
    public void updateCar(Car car) throws SQLException {
        String sql = "UPDATE cars SET makeModel=?, engineSize=?, fuelType=? WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, car.getMakeModel());
            stmt.setDouble(2, car.getEngineSize());
            String fuelTypeStr = car.fuelType.toString();
            stmt.setString(3, fuelTypeStr);
            stmt.setLong(4, car.getId());
            stmt.executeUpdate();  
            
            System.err.println("Statement executed...");
        }
    }
}
