/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CarsSQLServer;


/**
 *
 * @author 1796146
 */
public class Car {
    long id;
    String makeModel;
    Double engineSize;
    FuelType fuelType;
    enum FuelType { Gasoline, Diesel, Propane, Other };
    
    @Override
    public String toString() {
        return id + " " + makeModel + " " + engineSize + " " + fuelType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMakeModel() {
        return makeModel;
    }

    public void setMakeModel(String makeModel) {
        this.makeModel = makeModel;
    }

    public Double getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(Double engineSize) {
        this.engineSize = engineSize;
    }


    
    
}
