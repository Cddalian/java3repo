package ipd12.accountdb;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Transaction {

    public Transaction(long id, BigDecimal deposit, BigDecimal withdrawal, Date opDate) {
        setId(id);
        setDeposit(deposit);
        setWithdrawal(withdrawal);
        setOpDate(opDate);
    }
    
    
    
    private long id;
    private BigDecimal deposit;
    private BigDecimal withdrawal;
    private Date opDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getDeposit() {
        return deposit;
    }

    public void setDeposit(BigDecimal deposit) {
        if (deposit.compareTo(new BigDecimal(0)) < 0) {
            throw new IllegalArgumentException("Deposit must be a non-negative value");
        }
        this.deposit = deposit;
    }

    public BigDecimal getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(BigDecimal withdrawal) {
        if (deposit.compareTo(new BigDecimal(0)) < 0) {
            throw new IllegalArgumentException("Withdrawal must be a non-negative value");
        }
        this.withdrawal = withdrawal;
    }

    public Date getOpDate() {
        return opDate;
    }

    public void setOpDate(Date opDate) {
        this.opDate = opDate;
    }
    
    public static final SimpleDateFormat sdf = new SimpleDateFormat("MMM-d-yyyy");
    public static final SimpleDateFormat sdfCSV = new SimpleDateFormat("yyyy-MM-dd");
    
    
    @Override
    public String toString() {
        String dateStr = sdf.format(opDate);
        return String.format("%d: +%.2f -%.2f on %s", id, deposit, withdrawal, dateStr);
    }
    
    public String toCSV() {
        String dateStr = sdfCSV.format(opDate);
        return String.format("%d;%.2f;%.2f;%s", id, deposit, withdrawal, dateStr);
    }
}
