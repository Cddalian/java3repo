package ipd12.accountdb;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

class RecordNotFoundException extends SQLException {

    public RecordNotFoundException() {
    }

    public RecordNotFoundException(String msg) {
        super(msg);
    }

    public RecordNotFoundException(String msg, Throwable cause) {
        super(msg, cause);
    }
}

public class Database {

    private final static String HOSTNAME = "localhost";
    private final static String DBNAME = "first";
    private final static String USERNAME = "root";
    private final static String PASSWORD = "root";

    private Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://" + HOSTNAME + "/" + DBNAME,
                USERNAME, PASSWORD);
    }

    public void addTransaction(Transaction trans) throws SQLException {
        String sql = "INSERT INTO transactions (deposit, withdrawal) VALUES (?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setBigDecimal(1, trans.getDeposit());
            stmt.setBigDecimal(2, trans.getWithdrawal());
            stmt.executeUpdate();
        }
    }

    public BigDecimal getBalance() throws SQLException {
        String sql = "SELECT SUM(deposit-withdrawal) as sum FROM transactions";
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                BigDecimal sum = result.getBigDecimal("sum");
                return sum;
            } else {
                return new BigDecimal(0); // no records found
            }
        }
    }

    public ArrayList<Transaction> getAllTransactions() throws SQLException {
        String sql = "SELECT * FROM transactions";
        ArrayList<Transaction> list = new ArrayList<>();

        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                BigDecimal deposit = result.getBigDecimal("deposit");
                BigDecimal withdrawal = result.getBigDecimal("withdrawal");
                java.util.Date opdate = result.getDate("opDate");
                Transaction trans = new Transaction(id, deposit, withdrawal, opdate);
                list.add(trans);
            }
        }
        return list;
    }

    public Transaction getTransactionById(int id) throws SQLException {
        // FIXME: Preapred statement is required if id may contain malicious SQL injection code
        String sql = "SELECT * FROM transactions WHERE id=" + id;

        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                BigDecimal deposit = result.getBigDecimal("deposit");
                BigDecimal withdrawal = result.getBigDecimal("withdrawal");
                java.util.Date opdate = result.getDate("opDate");
                Transaction trans = new Transaction(id, deposit, withdrawal, opdate);
                return trans;
            } else {
                throw new RecordNotFoundException("Not found id=" + id);
                // return null;
            }
        }
    }

    public void updateTransaction(Transaction trans) throws SQLException {
        String sql = "UPDATE transactions SET task=?, dueDate=?, isDone=? WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setBigDecimal(1, trans.getDeposit());
            stmt.setBigDecimal(2, trans.getWithdrawal());
            stmt.setDate(3, new java.sql.Date(trans.getOpDate().getTime()));
            stmt.executeUpdate();
        }
    }

    public void deleteTransactionById(int id) throws SQLException {
        String sql = "DELETE FROM transactions WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);
            stmt.executeUpdate();
        }
    }

}
